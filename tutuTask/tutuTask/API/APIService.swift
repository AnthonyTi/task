//
//  APIService.swift
//  tutuTask
//
//  Created by Anthony Krasnov on 11/11/2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import Foundation
import Alamofire

class APIService {
    
    typealias EpisodeDownloadCompleteTuple = (fileUrl: String, episodeTitle: String)
    
    let baseiTunesSearchURL = "https://itunes.apple.com/search?"
    
    //singleton
    static let shared = APIService()
    
    func fetchPodcasts(searchText: String, completionHandler: @escaping ([ItunesModel]?, Error?) -> ()) {
        let parameters = ["term": searchText, "media": "music", "entity": "song"]
        Alamofire.request(baseiTunesSearchURL, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (dataResponse) in
            if let err = dataResponse.error {
                print("Failed to contact itunes", err)
                completionHandler(nil, err)
                return
            }
            
            guard let data = dataResponse.data else { return }
            do {
//                let str = String(bytes: data, encoding: .utf8)
//                print(str ?? "")
                let searchResult = try JSONDecoder().decode(SearchResults.self, from: data)
                completionHandler(searchResult.results, nil)
            } catch let decodeErr {
                print("Failed to decode:", decodeErr)
            }
        }
    }
    
    struct SearchResults: Decodable {
        let resultCount: Int
        let results: [ItunesModel]
    }
    
}
