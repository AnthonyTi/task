//
//  ViewController.swift
//  tutuTask
//
//  Created by Anthony Krasnov on 11/11/2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import UIKit

class ViewController: UITableViewController, UISearchBarDelegate {

    let cellId = "cellId"
    
    var itunes: [ItunesModel] = []
    
    let searchController = UISearchController(searchResultsController: nil)
    
    let errorLabel: UILabel = {
        let label = UILabel()
        label.text = "Something went wrong. Please try again later"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.allowsDefaultTighteningForTruncation = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "iTunes API"
        
        setupSearchBar()
        setupTableView()
        
    }

    func updateItunes (itunes: [ItunesModel]) {
        
    }
    
    //MARK:- Setups
    
    fileprivate func setupSearchBar() {
        self.definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
    }
    
    var timer: Timer?
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
            APIService.shared.fetchPodcasts(searchText: searchText) { (itunes, error) in
                if let err = error {
                    print(err)
                let alert = UIAlertController(title: "Network error", message: "Something went wrong. Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            print("default")
                            
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                            
                        }}))
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                }
                if let itunes = itunes {
                    self.itunes = itunes
                    self.tableView.reloadData()
                    self.updateItunes(itunes: itunes)
                }
            }
        })
    }
    
    fileprivate func setupTableView() {
        let nib = UINib(nibName: "ItunesCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicatorView.color = .darkGray
        activityIndicatorView.startAnimating()
        return activityIndicatorView
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = "Please enter a Search Term"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.itunes.count > 0 ? 0 : 250
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return self.itunes.isEmpty ? 200 : 0
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itunes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ItunesCell
    
            let itunes = self.itunes[indexPath.row]
            cell.itunes = itunes
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let it = itunes[indexPath.row]
        let moreInfoVC = MoreDetailsViewController(nibName: "MoreDetailsViewController", bundle: nil)
            moreInfoVC.itunesModel = it
        moreInfoVC.navigationItem.title = it.artistName ?? ""
        navigationController?.pushViewController(moreInfoVC, animated: true)
    }

}

