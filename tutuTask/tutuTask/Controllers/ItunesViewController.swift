//
//  ItunesViewController.swift
//  tutuTask
//
//  Created by Anthony Krasnov on 13/11/2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import UIKit
import CoreData

class ItunesViewController: ViewController {

    var container: NSPersistentContainer? = CoreDataStack.sharedInstance.persistentContainer
    
    override func updateItunes(itunes: [ItunesModel]) {
        super.updateItunes(itunes: itunes)
        updateDataBase(with: itunes)
    }
    
    private func updateDataBase(with itunes: [ItunesModel]) {
        container?.performBackgroundTask{ [weak self] context in
            try? ItunesMdl.clearTable(in: context)
            for itns in itunes {
                _ = try? ItunesMdl.createItunes(matching: itns, in: context)
            }
            try? context.save()
            self?.printDataBaseStat()
            self?.fetchAllItunes()
        }
    }
    
    private func printDataBaseStat() {
        if let context = container?.viewContext {
            context.perform {
                if let itunesCount = try? context.count(for: ItunesMdl.fetchRequest()) {
                    print("\(itunesCount) itunes songs")
                }
            }
        }
    }
    
    private func fetchAllItunes() {
        if let context = container?.viewContext {
            context.perform {
                if let itunez: [ItunesModel] = try? ItunesMdl.fetchAll(int: context)  {
                    itunez.forEach({ (itns) in
                        print("\(itns.artistName ?? "") \(itns.trackName ?? "")")
                    })
                    self.itunes = itunez
                    self.tableView.reloadData()
                }
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAllItunes()
        
    }
}
