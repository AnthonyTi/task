//
//  MoreDetailsViewController.swift
//  tutuTask
//
//  Created by Anthony Krasnov on 12/11/2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import UIKit

class MoreDetailsViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    var itunesModel: ItunesModel!
    
    override func viewDidLoad() {
        trackNameLabel.text = itunesModel.trackName ?? ""
        artistNameLabel.text = itunesModel.artistName ?? ""
        priceLabel.text = "\(itunesModel.trackPrice ?? 0) \(itunesModel.currency ?? "")"
        
        let url = URL(string: itunesModel.artworkUrl100?.toSecureHTTPS() ?? "")
        imageView.sd_setImage(with: url)
    }


}
