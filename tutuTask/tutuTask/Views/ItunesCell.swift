//
//  PodcastCell.swift
//  PodcastCourseLBTA
//
//  Created by Anthony Krasnov on 17.03.2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import UIKit
import SDWebImage

class ItunesCell: UITableViewCell {
    

    @IBOutlet weak var trackImageView: UIImageView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var episodeCountLabel: UILabel!
    
    var itunes: ItunesModel! {
        didSet {
            
            trackNameLabel.text = itunes.trackName
            artistNameLabel.text = itunes.artistName
            
            episodeCountLabel.text = "\(itunes.trackCount ?? 0) tracks"
            
            guard let url = URL(string: itunes.artworkUrl100 ?? "") else {return}
            trackImageView.sd_setImage(with: url, completed: nil)
        }
    }
    
}
