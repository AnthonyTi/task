//
//  ItunesModel.swift
//  tutuTask
//
//  Created by Anthony Krasnov on 11/11/2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import Foundation

class ItunesModel: NSObject, Decodable, NSCoding {
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(trackName ?? "", forKey: "trackNameKey")
        aCoder.encode(artistName ?? "", forKey: "artistNameKey")
        aCoder.encode(artworkUrl100 ?? "", forKey: "artworkUrl100Key")
        aCoder.encode(trackId ?? "", forKey: "trackIdKey")
        aCoder.encode(artistId ?? "", forKey: "artistIdKey")
        aCoder.encode(collectionId ?? "", forKey: "collectionIdKey")
        aCoder.encode(collectionPrice ?? "", forKey: "collectionPriceKey")
        aCoder.encode(trackPrice ?? "", forKey: "trackPriceKey")
        aCoder.encode(currency ?? "", forKey: "currencyKey")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.trackName = aDecoder.decodeObject(forKey: "trackNameKey") as? String
        self.artistName = aDecoder.decodeObject(forKey: "artistNameKey") as? String
        self.artworkUrl100 = aDecoder.decodeObject(forKey: "artworkUrl100Key") as? String
        self.trackId = aDecoder.decodeObject(forKey: "trackIdKey") as? Int64
        self.artistId = aDecoder.decodeObject(forKey: "artistIdKey") as? Int64
        self.collectionId = aDecoder.decodeObject(forKey: "collectionIdKey") as? Int64
        self.collectionPrice = aDecoder.decodeObject(forKey: "collectionPriceKey") as? Double
        self.trackPrice = aDecoder.decodeObject(forKey: "trackPriceKey") as? Double
        self.currency = aDecoder.decodeObject(forKey: "currencyKey") as? String
    }
    
    init(itunesMdl: ItunesMdl) {
        self.artistId = itunesMdl.artistId
        self.artistName = itunesMdl.artistName
        self.artworkUrl100 = itunesMdl.artworkUrl100
        self.collectionId = itunesMdl.collectionId
        self.collectionPrice = itunesMdl.collectionPrice
        self.currency = itunesMdl.currency
        self.trackCount = itunesMdl.trackCount
        self.trackName = itunesMdl.trackName
        self.trackId = itunesMdl.trackId
        self.trackPrice = itunesMdl.trackPrice
    }
    
    var trackName: String?
    var artistName: String?
    var artworkUrl100: String?
    var trackCount: Int16?
    var artistId: Int64?
    var collectionId: Int64?
    var trackId: Int64?
    var collectionPrice: Double?
    var trackPrice: Double?
    var currency: String?

}
