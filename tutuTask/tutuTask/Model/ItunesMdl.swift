//
//  ItunesMdl.swift
//  tutuTask
//
//  Created by Anthony Krasnov on 13/11/2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import UIKit
import CoreData

class ItunesMdl: NSManagedObject {
    class func createItunes(matching itunesInfo: ItunesModel, in context: NSManagedObjectContext) throws -> ItunesMdl {
        let request: NSFetchRequest<ItunesMdl> = ItunesMdl.fetchRequest()
        request.predicate = NSPredicate(format: "trackId = %@", NSNumber(value: itunesInfo.trackId!))
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1, "ItunesMdl.findOrCreateItunes -- database inonsistency")
                return matches[0]
            }
        } catch {
            throw error
        }
        
        let itunesMdl = ItunesMdl(context: context)
        itunesMdl.artistId = itunesInfo.artistId!
        itunesMdl.artistName = itunesInfo.artistName
        itunesMdl.artworkUrl100 = itunesInfo.artworkUrl100
        itunesMdl.collectionId = itunesInfo.collectionId!
        itunesMdl.collectionPrice = itunesInfo.collectionPrice ?? 0
        itunesMdl.currency = itunesInfo.currency
        itunesMdl.trackCount = itunesInfo.trackCount!
        itunesMdl.trackName = itunesInfo.trackName
        itunesMdl.trackPrice = itunesInfo.trackPrice ?? 0
        itunesMdl.trackId = itunesInfo.trackId ?? 0
        
        return itunesMdl
    }
    
    class func clearTable(in context: NSManagedObjectContext) throws {
        let request : NSFetchRequest<NSFetchRequestResult> = ItunesMdl.fetchRequest()
        let deleteAll = NSBatchDeleteRequest(fetchRequest: request)
        do {
            try context.execute(deleteAll)
        } catch {
            throw error
        }
    }
    
    class func fetchAll(int context: NSManagedObjectContext) throws -> [ItunesModel] {
        let request: NSFetchRequest<ItunesMdl> = ItunesMdl.fetchRequest()
        var itunezz = [ItunesModel]()
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                matches.forEach { (itnsMdl) in
                    itunezz.append(ItunesModel(itunesMdl: itnsMdl))
                }
            }
        } catch {
            throw error
        }
        return itunezz
    }
}
