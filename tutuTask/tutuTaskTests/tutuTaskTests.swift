//
//  tutuTaskTests.swift
//  tutuTaskTests
//
//  Created by Anthony Krasnov on 11/11/2018.
//  Copyright © 2018 Anthony Krasnov. All rights reserved.
//

import XCTest
@testable import tutuTask

class tutuTaskTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testValidCallToItunes() {
        // given
        let text = "abba"
        let promise = expectation(description: "Simple request")
        // when
        
        APIService.shared.fetchPodcasts(searchText: text) { (itunez, error) in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            }
            if let _ = itunez {
                XCTAssert(true)
                promise.fulfill()
                
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }

}
